import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';


@Injectable()
export class AirtableService {

  constructor(private http:Http) { 

  		console.log("service started");
  }

  private Url='https://validator.w3.org/nu/?doc=http://navgurukul.org/&out=json';
  getRecords()
  {
  		return this.http
  		.get("https://api.airtable.com/v0/apphOVYXkHXl8pOqE/Students?api_key=keyZCvxOTT9NwpVIi")
  		.map(res => res.json().records);

  		//return this.http.post("https://validator.w3.org/check?uri=http://navgurukul.org/&output=json")
  		//.map(res => res.json());
  }

  getResult()
  {

  		return this.http.get(this.Url).map(res => res.json().messages);
  }



  // getResult()
  // {

  // 	let headers = new Headers({'content-type': 'json'});
  // 	let options = new RequestOptions({headers:headers});

  // 	return this.http.post(this.Url,"<!doctype html><html></html>",options).map(res => res.json());	
  // }
}
