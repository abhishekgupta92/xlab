/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { JsLintComponent } from './js-lint.component';

describe('JsLintComponent', () => {
  let component: JsLintComponent;
  let fixture: ComponentFixture<JsLintComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JsLintComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JsLintComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
