import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import  { AirtableService } from './airtable.service';
import  { JsBinService } from './js-bin.service';
import { StudentInfoComponent } from './student-info/student-info.component';
import { CssLintComponent } from './css-lint/css-lint.component';
import { JsLintComponent } from './js-lint/js-lint.component';


@NgModule({	
  declarations: [
    AppComponent,
    StudentInfoComponent,
    CssLintComponent,
    JsLintComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [AirtableService, JsBinService],
  bootstrap: [AppComponent]
})
export class AppModule { }
