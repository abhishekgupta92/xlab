import { Component, OnInit } from '@angular/core';
import { JsBinService } from '../js-bin.service';
import * as parserlib from 'parserlib';
import * as jshint from 'jshint';


@Component({
  selector: 'app-css-lint',
  templateUrl: './css-lint.component.html',
  styleUrls: ['./css-lint.component.css']
})

export class CssLintComponent implements OnInit {
  
  private parser:any;
  css:string;
  
  constructor(private jsBinService:JsBinService) {
  	this.parser = new parserlib.css.Parser();
  	this.css="";
  }
  
  onProperty(event:any):void
	{
		if (event.property == "padding" && parseInt(event.value) > 20) {
			console.log("incorrect");
		}
	  console.log(`${event.property} ${(event.value)}  ${event.property.line} ${event.property.col} `);
	}
	onError(event:any):void
	{
  	console.log("Parse error: " + event.message + " (" + event.line + "," + event.col + ")", "error");
  }

  // filter(data:string):string
  // {

  // 	return data;
  // }
  ngOnInit() {
  	this.parser.addListener("error", this.onError);

    this.parser.addListener("property", this.onProperty);

    this.jsBinService.getBinCss().subscribe( data => { 
    	this.parser.parse(data);
      this.css=data;
    });
  //   this.parser.addListener("startstylesheet", function() {
  //     console.log("Starting to parse stylesheet");
		// });

		// this.parser.addListener("endstylesheet", function() {
	 //    console.log("Finished parsing stylesheet");
		// });
  }
}